package App.ECommerce.controller;

import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import App.ECommerce.dao.ItemDao;
import App.ECommerce.model.Item;

@Path("item")
public class ItemController {

	ItemDao itemdao = new ItemDao();
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public List<Item> getItems(){
		
		List<Item> items = itemdao.getItems();
		
		return items;
	}
	
	@GET
	@Path("/{id}")
	@Produces(MediaType.APPLICATION_JSON)
	public List<Item> getItem(@PathParam("id") int id){
		
		List<Item> item = itemdao.getItem(id);
		
		return item;
	}
	
	
	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response createItem(Item i){
		
		int itemId=0;
		
		itemId=itemdao.createItem(i);
		
		if(itemId>0){
			
			return Response.status(201).entity("Item got created").build();
		}
		
		else{
			
			return Response.status(500).entity("Item not created.").build();
		}
		
	}
	
	@DELETE
	@Path("/{id}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response deleteItem(@PathParam("id") int id){
		int flag=0;
		
		flag=itemdao.deleteItem(id);
		
		if(flag>0){
			return Response.status(200).entity("Item got deleted").build();
		}
		else{
			
			return Response.status(400).entity("Item doesnt exist").build();
		}
		
	}
	
}
