package App.ECommerce.dao;

import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;

import App.ECommerce.model.Item;
import App.ECommerce.model.Order;

public class OrderDao {

	
public List<Order> getOrders(){
		
		Session session = SessionUtil.getSession();
		
		String hql="From Order";
		Query q = session.createQuery(hql);
		
		List<Order> orders = q.list();
		
		session.close();
		return orders;
	}

public List<Order> getItem(int id){
	
	Session session = SessionUtil.getSession();
	
	
	String hql="From Order o where o.id= :id ";
	Query q = session.createQuery(hql);
	q.setParameter("id",id);
	
	List<Order> order = q.list();
	
	session.close();
	return order;
}

public int createOrder(Order o){
	Session session = SessionUtil.getSession();
	Transaction tx = session.beginTransaction();
	
	 int orderId=0;
	 
	 orderId= (int) session.save(o);
	 
	 tx.commit();
	 session.close();
	 
	 return orderId;
	 
	
	
}

}
