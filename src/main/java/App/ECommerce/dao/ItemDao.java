package App.ECommerce.dao;

import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;

import App.ECommerce.model.Item;

public class ItemDao {

	
	public List<Item> getItems(){
		
		Session session = SessionUtil.getSession();
		
		String hql="From Item";
		Query q = session.createQuery(hql);
		
		List<Item> items = q.list();
		
		session.close();
		return items;
	}
	
	public List<Item> getItem(int id){
		
		Session session = SessionUtil.getSession();
		
		
		String hql="From Item i where i.id= :id ";
		Query q = session.createQuery(hql);
		q.setParameter("id",id);
		
		List<Item> item = q.list();
		
		session.close();
		return item;
	}
	
	public int createItem(Item i){
		
		Session session = SessionUtil.getSession();
		Transaction tx = session.beginTransaction();
		
		 int itemId=0;
		 
		 itemId= (int) session.save(i);
		 
		 tx.commit();
		 session.close();
		 
		 return itemId;
		 
		
	}
	
	
	public int deleteItem(int id){
		
		Session session = SessionUtil.getSession();
		Transaction tx = session.beginTransaction();
		
		 int flag=0;
		 
		 Item i = (Item) session.load(Item.class,id);
		 if(i!=null){
		 session.delete(i);
		 flag=1;
		 }
		 
		 tx.commit();
		 session.close();
		 
		 return flag;
		
	}

	public int getItemQuantity(int id){
	
		int quantity=0;
		Session session = SessionUtil.getSession();
		
		 Item item = (Item) session.get(Item.class, new Integer(id));
		 session.close();
		 quantity=item.getQuantity();
		return quantity;
		 
		}
}
