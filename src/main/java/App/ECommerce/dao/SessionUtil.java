package App.ECommerce.dao;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Configuration;
import org.hibernate.service.ServiceRegistry;

public class SessionUtil {

	private static SessionUtil INSTANCE;
	private SessionFactory sessionFactory;
	
	private SessionUtil(){
		
		 Configuration configuration = new Configuration();
	        configuration.configure("hibernate.cfg.xml");
	        ServiceRegistry serviceRegistry = new StandardServiceRegistryBuilder().applySettings(configuration.getProperties()).build();
	        sessionFactory = configuration.buildSessionFactory(serviceRegistry); 
	}
	
	public static SessionUtil getInstance(){
		
		if(INSTANCE==null){
			INSTANCE = new SessionUtil();
		}
		
		return INSTANCE;
	}
	
	
	public static Session getSession(){
		
		return getInstance().sessionFactory.openSession();
	}
	
}
